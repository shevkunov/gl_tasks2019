#pragma once

#include <memory>

#include <cuda_runtime_api.h>

#include <common/CudaForwardDefines.h>

cudaError_t image_host_to_dev(const unsigned char* image, size_t size, int width, int height, int channels, std::shared_ptr<CudaImage4c> &d_image);
cudaError_t image_dev_to_host(std::shared_ptr<CudaImage4c> d_image, unsigned char* h_image);

std::shared_ptr<CudaImage4c> load_image_rgb(const char* path);
void save_image_rgb(const char* path, std::shared_ptr<CudaImage4c> d_image);


