#pragma once

#include <glm/glm.hpp>
#include <common/common_functions.cuh>
#include "rasterizer.h"

void cuda_processing(const RasterizerConfiguration &config, const std::vector<RasterizerPolygon> &polygons,
                     glm::u8vec4* dstImage, int* zBuffer, ImageWorkArea workArea, glm::uvec2 blockDim);

void cuda_processing_2(const RasterizerConfiguration &config, const std::vector<RasterizerPolygon> &polygons,
                       glm::u8vec4* dstImage, int* zBuffer, ImageWorkArea workArea, glm::uvec2 blockDim,
                       int threadsPerTriangle);
