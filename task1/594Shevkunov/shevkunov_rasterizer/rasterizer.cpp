#include "rasterizer.h"

#include "cuda_processing.cuh"

void rasterize(const RasterizerConfiguration &config, const std::vector<RasterizerPolygon> &polygons,
               const std::string &fname, glm::uvec2 blockSize, int kind,
               int threadsPerTriangle) {
    ImageWorkArea workArea(glm::uvec2(config.width, config.height));
    auto dstImage = std::make_shared<CudaImage4c>(config.width, config.height, ResourceLocation::Device);
    cudaMemset(dstImage->getHandle(), 0, dstImage->getSize());

    auto zBuffer = std::make_shared<CudaImage<int>>(config.width, config.height, ResourceLocation::Device);
    cudaMemset(zBuffer->getHandle(), 0xfe, dstImage->getSize());

    L_INFO << "Launched Kind = " << kind << ENDL;
    clock_t in = clock();
    switch (kind) {
        case 0:
            cuda_processing(config, polygons, dstImage->getHandle(), zBuffer->getHandle(), workArea, blockSize);
            break;
        default:
            cuda_processing_2(config, polygons, dstImage->getHandle(), zBuffer->getHandle(),
                              workArea, blockSize, threadsPerTriangle);
    }
    clock_t out = clock();
    L_INFO << "Done in " << double(out - in) / CLOCKS_PER_SEC << "s." << ENDL;
    save_image_rgb(fname.c_str(), dstImage);
}
