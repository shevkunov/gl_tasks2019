#include "rasterizer.h"
#include <vector>


std::vector<RasterizerPolygon> test_data() {
    glm::mat3 p1(glm::vec3(0, 40, -1), glm::vec3(60, 0, -1), glm::vec3(0., -40, -1));
    glm::mat3x4 c1(glm::vec4(255, 0, 0, 128), glm::vec4(0, 255, 0, 128), glm::vec4(0., 0, 255, 128));

    glm::mat3 p2(glm::vec3(-60, 20, -5), glm::vec3(51, 0, 0), glm::vec3(-60, -20, -5));
    glm::mat3x4 c2(glm::vec4(0, 255, 0, 255), glm::vec4(255, 0, 0, 255), glm::vec4(0, 0, 255, 255));

    return {
            RasterizerPolygon({p1, c1}),
            RasterizerPolygon({p2, c2})
    };
}

std::vector<RasterizerPolygon> test_data_2(int size = 3, bool uniform_triangles = true) {
    std::vector<RasterizerPolygon> data;

    const int img_side = 200;
    const int side = 400;

    const int tsize = 3;

    for (int i = 0; i < size; ++i) {
        int x = (i % img_side) * 4;
        int y = (i / img_side) * 4;

        glm::mat3 point(
            glm::vec3(x - side, y - side, -1),
            glm::vec3(x - side + tsize, y - side + tsize, -1),
            glm::vec3(x - side + tsize, y - side, -1)
        );
        glm::mat3x4 color(
            glm::vec4(x % 25 * 10, y % 25 * 10, 0, 150),
            glm::vec4((x + 1) % 25 * 10, (y + 1) % 25 * 10, 0, 150),
            glm::vec4((x + 1) % 25 * 10, y % 25 * 10, 255, 150)
        );

        data.emplace_back(RasterizerPolygon({point, color}));
    }

    glm::mat3 point(
            glm::vec3(-side / 2, -side / 2, -2),
            glm::vec3(+side / 2, +side / 2, -2),
            glm::vec3(-side / 2, +side / 2, -2)
    );
    glm::mat3x4 color(
            glm::vec4(255, 0, 0, 255),
            glm::vec4(0, 255, 0, 255),
            glm::vec4(0, 0, 255, 255)
    );

    if (!uniform_triangles) {
        data.emplace_back(RasterizerPolygon({point, color}));
    }
    return data;
}

int main() {
    printCudaInfo();
    cudaDeviceProp deviceProp;
    cudaGetDeviceProperties(&deviceProp, 0);

    const RasterizerConfiguration config({-400, -400, 800, 800});
    const auto test = test_data_2(40000, false);

    const std::vector<std::string> fnames = {
        "rasterized-linear.png",
        "rasterized-parallel.png",
    };
    for (int kind = 0; kind <= 1; ++kind) {
        rasterize(config, test, "./594ShevkunovData1/" + fnames[kind], glm::uvec2(32, 32), kind, 32); // , "./594ShevkunovData1/rasterized.png");
    }
}
