#include <host_defines.h>
#include <common/common_functions.cu>
#include <common/CudaError.h>
#include <common/CudaForwardDefines.h>

#include "cuda_processing.cuh"

#include "rasterizer.h"


__global__ void _cuda_processing(glm::u8vec4* dstImage, int* zBuffer, ImageWorkArea workArea, glm::ivec2 imgOffest,
                                 glm::mat3 triangle, glm::mat3x4 colors) {
    glm::ivec2 workPixel(threadIdx.x + blockDim.x * blockIdx.x, threadIdx.y + blockDim.y * blockIdx.y);
    workPixel = workPixel + imgOffest;

    int offset = count_img_offset(workPixel, workArea);

    if (offset < 0) {
        return;
    }

    const glm::mat2 forward_baricentric_matrix(triangle[0] - triangle[2], triangle[1] - triangle[2]);
    const glm::vec2 decart_pos = glm::vec2(workPixel) - glm::vec2(triangle[2]);
    const glm::vec2 baricentric_2d = glm::inverse(forward_baricentric_matrix) * decart_pos;
    const glm::vec3 baricentric_3d(baricentric_2d.x, baricentric_2d.y, 1. - baricentric_2d.x - baricentric_2d.y);

    const float eps = 1e-5;
    const glm::vec3 bound(-eps, -eps, -eps);

    if (!glm::all(glm::greaterThan(baricentric_3d, bound))) {
        if (DRAW_BOUNDING_BOX) {
            dstImage[offset] = glm::u8vec4(0, 0, 0, 255);
        }
        return;
    }

    const glm::vec3 source_point = triangle * baricentric_3d;

    const int zFactor = 1000;
    const int z = int(source_point.z * zFactor);
    if (z >= zBuffer[offset]) {
        zBuffer[offset] = z;

        const glm::vec4 color = colors * baricentric_3d;
        dstImage[offset] = glm::u8vec4(color);
    }

}


void cuda_draw_triangle(glm::u8vec4* dstImage, int* zBuffer, ImageWorkArea workArea, glm::uvec2 blockDim,
                        const RasterizerPolygon &triangle) {
    const glm::mat3 &points = triangle.points;

    glm::ivec2 min_bound(
            std::min(std::min(points[0].x, points[1].x), points[2].x) - 1,
            std::min(std::min(points[0].y, points[1].y), points[2].y) - 1
    );

    glm::ivec2 max_bound(
            std::max(std::max(points[0].x, points[1].x), points[2].x) + 1,
            std::max(std::max(points[0].y, points[1].y), points[2].y) + 1
    );

    glm::uvec2 bounding_box = glm::uvec2(max_bound - min_bound);

    dim3 blkDim(blockDim.x, blockDim.y, 1);
    glm::uvec2 gridDim = glm::uvec2(glm::ivec2(bounding_box + blockDim) - 1) / blockDim;
    dim3 grdDim = { gridDim.x, gridDim.y, 1 };
    _cuda_processing<<<grdDim, blkDim>>>(dstImage, zBuffer, workArea, min_bound, triangle.points, triangle.colors);
    checkCudaErrors(cudaGetLastError());
}

void cuda_processing(const RasterizerConfiguration &config,  const std::vector<RasterizerPolygon> &polygons,
                     glm::u8vec4* dstImage, int* zBuffer,
                     ImageWorkArea workArea, glm::uvec2 blockDim) {
    clock_t in = clock();

    glm::vec3 shift(-config.min_x, -config.min_y, 0);
    glm::mat3 shift_matrix(shift, shift, shift);

    for (RasterizerPolygon poly : polygons) {
        poly.points += shift_matrix;
        cuda_draw_triangle(dstImage, zBuffer, workArea, blockDim, poly);
    }
    checkCudaErrors(cudaGetLastError());


    clock_t out = clock();
    L_INFO << "Only rasterization done in " << double(out - in) / CLOCKS_PER_SEC << "s." << ENDL;
}


__global__ void _cuda_processing_2(glm::u8vec4* dstImage, int* zBuffer, ImageWorkArea workArea,
                                   RasterizerPolygon* handle, bool CASorWrite, int threadsPerTriangle) {
    glm::ivec2 wp(threadIdx.x + blockDim.x * blockIdx.x, threadIdx.y + blockDim.y * blockIdx.y);

    const RasterizerPolygon &triangle = handle[wp.x];
    const int skipRate = wp.y;

    const glm::mat3 &points = triangle.points;

    auto min = [](int a, int b, int c) {
        int t = (a < b) ? a : b;
        return (t < c) ? t : c;
    };

    auto max = [](int a, int b, int c) {
        int t = (a > b) ? a : b;
        return (t > c) ? t : c;
    };

    glm::ivec2 min_bound(
            min(points[0].x, points[1].x, points[2].x) - 1,
            min(points[0].y, points[1].y, points[2].y) - 1
    );

    glm::ivec2 max_bound(
            max(points[0].x, points[1].x, points[2].x) + 1,
            max(points[0].y, points[1].y, points[2].y) + 1
    );

    glm::uvec2 bounding_box = glm::uvec2(max_bound - min_bound);

    for (int i = skipRate; i < bounding_box.x * bounding_box.y; i += threadsPerTriangle) {
        glm::ivec2 workPixel(i % bounding_box.x + min_bound.x, i / bounding_box.x + min_bound.y);
        int offset = count_img_offset(workPixel, workArea);

        if (offset < 0) {
            continue;
        }

        const glm::mat2 forward_baricentric_matrix(points[0] - points[2], points[1] - points[2]);
        const glm::vec2 decart_pos = glm::vec2(workPixel) - glm::vec2(points[2]);
        const glm::vec2 baricentric_2d = glm::inverse(forward_baricentric_matrix) * decart_pos;
        const glm::vec3 baricentric_3d(baricentric_2d.x, baricentric_2d.y, 1. - baricentric_2d.x - baricentric_2d.y);

        const float eps = 1e-5;
        const glm::vec3 bound(-eps, -eps, -eps);

        if (!glm::all(glm::greaterThan(baricentric_3d, bound))) {
            if (DRAW_BOUNDING_BOX) {
                dstImage[offset] = glm::u8vec4(0, 0, 0, 255);
            }
            continue;
        }

        const glm::vec3 source_point = points * baricentric_3d;

        const int zFactor = 1000;
        const int z = int(source_point.z * zFactor);
        if (CASorWrite) {
            atomicMax(&zBuffer[offset], z);
        } else {
            if (zBuffer[offset] == z) {
                const glm::vec4 color = triangle.colors * baricentric_3d;
                dstImage[offset] = glm::u8vec4(color);
            }
        }
    }
}

void cuda_processing_2(const RasterizerConfiguration &config,  const std::vector<RasterizerPolygon> &polygons,
                       glm::u8vec4* dstImage, int* zBuffer,
                       ImageWorkArea workArea, glm::uvec2 blockDim, int threadsPerTriangle) {
    glm::vec3 shift(-config.min_x, -config.min_y, 0);
    glm::mat3 shift_matrix(shift, shift, shift);

    RasterizerPolygon* handle;
    checkCudaErrors(cudaMalloc(reinterpret_cast<void**>(&handle), sizeof(RasterizerPolygon) * polygons.size()));
    for (size_t i = 0; i < polygons.size(); ++i) {
        RasterizerPolygon poly = polygons[i];
        poly.points += shift_matrix;
        cudaMemcpy(&handle[i], &poly, sizeof(RasterizerPolygon), cudaMemcpyHostToDevice);
    }


    clock_t in = clock();
    glm::uvec2 bounding_box(polygons.size(), threadsPerTriangle);

    dim3 blkDim(blockDim.x, blockDim.y, 1);
    glm::uvec2 gridDim = glm::uvec2(glm::ivec2(bounding_box + blockDim) - 1) / blockDim;
    dim3 grdDim = { gridDim.x, gridDim.y, 1 };
    _cuda_processing_2<<<grdDim, blkDim>>>(dstImage, zBuffer, workArea, handle, true, threadsPerTriangle);
    _cuda_processing_2<<<grdDim, blkDim>>>(dstImage, zBuffer, workArea, handle, false, threadsPerTriangle);
    checkCudaErrors(cudaGetLastError());

    clock_t out = clock();
    L_INFO << "Only rasterization done in " << double(out - in) / CLOCKS_PER_SEC << "s." << ENDL;

    checkCudaErrors(cudaFree(handle));
}
