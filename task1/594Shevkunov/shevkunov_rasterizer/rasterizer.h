#pragma once

#include <cudasoil/cuda_soil.h>

#include <common/CudaImage.h>
#include <common/CudaInfo.h>
#include <common/common_functions.cuh>

#include <glm/glm.hpp>
#include <iostream>
#include <vector>
#include <ctime>

#define L_INFO std::cerr
#define ENDL std::endl
#define DRAW_BOUNDING_BOX 0

#define DEFAULT_BLOCK_SIZE glm::uvec2(4, 4)
#define DEFAULT_KIND 0
#define DEFAULT_THREAD_PER_TRIANGLE 40


struct RasterizerConfiguration {
    const int min_x;
    const int min_y;

    const int width;
    const int height;
};

struct RasterizerPolygon {
    glm::mat3 points;
    glm::mat3x4 colors;
};

void rasterize(const RasterizerConfiguration &config, const std::vector<RasterizerPolygon> &polygons,
               const std::string &fname = "rasterized.png", glm::uvec2 blockSize = DEFAULT_BLOCK_SIZE,
               int kind = DEFAULT_KIND,
               int threadsPerTriangle = DEFAULT_THREAD_PER_TRIANGLE);
