/**
* Шейдер для отрисовки маркера источника света
*/

#version 330
uniform vec4 color;
out vec4 fragOutColor;

void main() {
	fragOutColor = color;
}
