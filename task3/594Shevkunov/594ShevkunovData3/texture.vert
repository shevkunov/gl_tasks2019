/**
* Преобразует координаты вершины и нормально в систему координат виртуальной камеры и передает на выход.
* Копирует на выход текстурные координаты.
*/

#version 330

// матрицы для преобразования координат
uniform mat4 modelMatrix;  // локальная -> мировая
uniform mat4 viewMatrix;  // мировая -> камеры
uniform mat4 projectionMatrix;  // камеры -> усечённые

// матрица для преобразования нормалей (локальная -> камеры)
uniform mat3 normalToCameraMatrix;

layout(location = 0) in vec3 vertexPosition; // координаты в локальной системе координат
layout(location = 1) in vec3 vertexNormal; // нормаль в локальной системе координат
layout(location = 2) in vec2 vertexTexCoord; //текстурные координаты вершины

out vec3 normalCamSpace;
out vec4 posCamSpace;
out vec2 texCoord;

void main() {
	texCoord = vertexTexCoord;

	posCamSpace = viewMatrix * modelMatrix * vec4(vertexPosition, 1.0);  // преобразование координат вершины в систему координат камеры
	normalCamSpace = normalize(normalToCameraMatrix * vertexNormal);  // преобразование нормали в систему координат камеры
	
	gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(vertexPosition, 1.0);
}
