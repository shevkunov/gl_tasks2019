#include "util.hpp"


MeshPtr makeFloorBlock(float size) {
	std::vector<glm::vec3> vertexes;
	std::vector<glm::vec3> normals;
	std::vector<glm::vec2> texcoords;

    vertexes.emplace_back(-size, size, -size);
    vertexes.emplace_back(size, -size, -size);
    vertexes.emplace_back(size, size, -size);

    normals.emplace_back(0., 0., 1.);
    normals.emplace_back(0., 0., 1.);
    normals.emplace_back(0., 0., 1.);

    texcoords.emplace_back(0.0, 1.0);
    texcoords.emplace_back(1.0, 0.0);
    texcoords.emplace_back(1.0, 1.0);

    vertexes.emplace_back(-size, size, -size);
    vertexes.emplace_back(-size, -size, -size);
    vertexes.emplace_back(size, -size, -size);

    normals.emplace_back(0.0, 0.0, 1.);
    normals.emplace_back(0.0, 0.0, 1.);
    normals.emplace_back(0.0, 0.0, 1.);

    texcoords.emplace_back(0.0, 1.0);
    texcoords.emplace_back(0.0, 0.0);
    texcoords.emplace_back(1.0, 0.0);

    assert(vertexes.size() == normals.size());
    assert(vertexes.size() == texcoords.size());

	DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	buf0->setData(vertexes.size() * sizeof(glm::vec3), vertexes.data());

	DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	buf1->setData(normals.size() * sizeof(glm::vec3), normals.data());

	DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	buf2->setData(texcoords.size() * sizeof(glm::vec2), texcoords.data());

	MeshPtr mesh = std::make_shared<Mesh>();
	mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
	mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
	mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
	mesh->setPrimitiveType(GL_TRIANGLES);
	mesh->setVertexCount(vertexes.size());

	return mesh;
}

