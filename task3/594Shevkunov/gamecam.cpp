#include "gamecam.hpp"

#define GLM_ENABLE_EXPERIMENTAL

#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>
#include <imgui.h>

#include <iostream>
#include <utility>


const float PlayerHeight = 0.2;

PlayerCameraMover::PlayerCameraMover(TLevelMap& levelMap)
    : CameraMover()
    , _pos(glm::vec3(levelMap.begin.x + levelMap.startX, levelMap.begin.y + levelMap.startY, PlayerHeight))
    , _levelMap(levelMap)
    , teleported(false)
    , madeTeleportHere(false)
{
    //Нам нужно как-нибудь посчитать начальную ориентацию камеры
    _rot = glm::toQuat(glm::lookAt(_pos, glm::vec3(0.0f, 0.0f, 0.5f), glm::vec3(0.0f, 0.0f, 1.0f)));
}

void PlayerCameraMover::handleKey(GLFWwindow* window, int key, int scancode, int action, int mods) {
}

void PlayerCameraMover::openTeleport() {
    if (!madeTeleportHere) {
        if (_levelMap.portals.size() == 2) {
            _levelMap.portals[0] = _levelMap.portals[1];
            _levelMap.portals.pop_back();
        }

        glm::vec3 forwDir = glm::vec3(0.0f, 0.0f, -1.0f) * _rot;

        // Не двигаемся по высоте
        double dir = (fabs(forwDir.x) > fabs(forwDir.y)) ? 1. : 0.;

        double dx = forwDir.x / fabs(forwDir.x) * dir;
        double dy = forwDir.y / fabs(forwDir.y) * (1. - dir);

        double fwdX = _pos.x + 0.5 * dx;
        double fwdY = _pos.y + 0.5 * dy;

        double blockX = int(fwdX - _levelMap.begin.x + 0.5) + _levelMap.begin.x;
        double blockY = int(fwdY - _levelMap.begin.y + 0.5) + _levelMap.begin.y;

        if (_levelMap.collide(glm::vec2(fwdX, fwdY))) {
            auto second = TPortalPos(blockX - 0.05f * dx,
                                     blockY - 0.05f * dy,
                                     blockX - 0.55f * dx,
                                     blockY - 0.55f * dy,
                                     blockX - 0.6f * dx,
                                     blockY - 0.6f * dy);
            auto addTeleport = [&]() {
                madeTeleportHere = true;
                teleported = true;
                _levelMap.portals.emplace_back(second);
            };

            if (!_levelMap.portals.empty()) {
                const auto first = _levelMap.portals[0];
                if (fabs(first.x - second.x) + fabs(first.y - second.y) > 0.2) {
                    addTeleport();
                }
            } else {
                addTeleport();
            }
        }
    }
}

void PlayerCameraMover::handleMouseMove(GLFWwindow* window, double xpos, double ypos) {
    if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS) {
        double dx = xpos - _oldXPos;
        double dy = ypos - _oldYPos;

        //Добавляем небольшой поворот вверх/вниз
        glm::vec3 rightDir = glm::vec3(1.0f, 0.0f, 0.0f) * _rot;
        _rot *= glm::angleAxis(static_cast<float>(dy * 0.005), rightDir);

        //Добавляем небольшой поворов вокруг вертикальной оси
        glm::vec3 upDir(0.0f, 0.0f, 1.0f);
        _rot *= glm::angleAxis(static_cast<float>(dx * 0.005), upDir);
    }

    if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT) == GLFW_PRESS) {
        openTeleport();
    }

    _oldXPos = xpos;
    _oldYPos = ypos;
}

void PlayerCameraMover::handleScroll(GLFWwindow* window, double xoffset, double yoffset) {
}

void PlayerCameraMover::update(GLFWwindow* window, double dt) {
    if (glfwGetKey(window, GLFW_KEY_EQUAL) == GLFW_PRESS) {
        _speed += .1f;
    }

    if (glfwGetKey(window, GLFW_KEY_MINUS) == GLFW_PRESS) {
        _speed -= .1f;
    }

    glm::vec3 forwDir = glm::vec3(0.0f, 0.0f, -1.0f) * _rot;
    glm::vec3 rightDir = glm::vec3(1.0f, 0.0f, 0.0f) * _rot;

    // Не двигаемся по высоте
    forwDir.z = 0;
    rightDir.z = 0;

    //Двигаем камеру вперед/назад
    auto newPos = _pos;

    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
        newPos += forwDir * _speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
        newPos -= forwDir * _speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
        newPos -= rightDir * _speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
        newPos += rightDir * _speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_P) == GLFW_PRESS) {
        openTeleport();
    }


    if (!_levelMap.collide(glm::vec2(newPos.x, newPos.y))) {
        if (_pos.x != newPos.x && _pos.y != newPos.y) {
            madeTeleportHere = false;
        }
        _pos = newPos;
    }

    if (!madeTeleportHere && (_levelMap.portals.size() == 2)) {
        auto collide = [](glm::vec3 pos, glm::vec3 otherPos) {
            return (fabs(pos.x - otherPos.x) < 0.6) && (fabs(pos.y - otherPos.y) < 0.6);
        };
        auto portalInPos = glm::vec3(_levelMap.portals[0].x, _levelMap.portals[0].y, PlayerHeight);
        auto portalOutPos = glm::vec3(_levelMap.portals[1].x, _levelMap.portals[1].y, PlayerHeight);

        const bool collidePortalIn = collide(_pos, portalInPos);
        const bool collidePortalOut = collide(_pos, portalOutPos);

        if (!(collidePortalIn || collidePortalOut)) {
            teleported = false;
        }

        if (!teleported && collidePortalIn) {
            _pos = glm::vec3(_levelMap.portals[1].at_x, _levelMap.portals[1].at_y, PlayerHeight);
            teleported = true;
        }

        if (!teleported && collidePortalOut) {
            _pos = glm::vec3(_levelMap.portals[0].at_x, _levelMap.portals[0].at_y, PlayerHeight);
            teleported = true;
        }
    }

    //-----------------------------------------

    //Соединяем перемещение и поворот вместе
    _camera.viewMatrix = glm::toMat4(-_rot) * glm::translate(-_pos);

    //-----------------------------------------

    int width, height;
    glfwGetFramebufferSize(window, &width, &height);

    //Обновляем матрицу проекции на случай, если размеры окна изменились
    _camera.projMatrix = glm::perspective(glm::radians(45.0f), (float)width / height, _near, _far);
}



void FreeCameraSpeedableMover::update(GLFWwindow* window, double dt) {
    if (glfwGetKey(window, GLFW_KEY_EQUAL) == GLFW_PRESS) {
        _speed += .1f;
    }

    if (glfwGetKey(window, GLFW_KEY_MINUS) == GLFW_PRESS) {
        _speed -= .1f;
    }

    //Получаем текущее направление "вперед" в мировой системе координат
    glm::vec3 forwDir = glm::vec3(0.0f, 0.0f, -1.0f) * _rot;

    //Получаем текущее направление "вправо" в мировой системе координат
    glm::vec3 rightDir = glm::vec3(1.0f, 0.0f, 0.0f) * _rot;

    //Двигаем камеру вперед/назад
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
        _pos += forwDir * _speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
        _pos -= forwDir * _speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
        _pos -= rightDir * _speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
        _pos += rightDir * _speed * static_cast<float>(dt);
    }

    //-----------------------------------------

    //Соединяем перемещение и поворот вместе
    _camera.viewMatrix = glm::toMat4(-_rot) * glm::translate(-_pos);

    //-----------------------------------------

    int width, height;
    glfwGetFramebufferSize(window, &width, &height);

    //Обновляем матрицу проекции на случай, если размеры окна изменились
    _camera.projMatrix = glm::perspective(glm::radians(45.0f), (float)width / height, _near, _far);
}
