set(SRC_FILES
    main.cpp
    common/Application.cpp
    common/Camera.cpp
    common/Mesh.cpp
    common/DebugOutput.cpp
    common/ShaderProgram.cpp
    common/Texture.cpp
    labapp.cpp
    gamecam.cpp
    util.cpp
    portal.cpp
)

MAKE_OPENGL_TASK(594Shevkunov 3 "${SRC_FILES}")

if (UNIX)
    target_link_libraries(594Shevkunov3 stdc++fs)
endif()
