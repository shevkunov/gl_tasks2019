#include "portal.hpp"

CameraInfo getPortalCameraInfo(glm::vec3 pos, glm::vec3 center, glm::vec3 up, float perspective, float width, float height, float _near, float _far) {
    auto rot = glm::toQuat(glm::lookAt(pos, center, up));
    CameraInfo camera;
    camera.viewMatrix = glm::toMat4(-rot) * glm::translate(-pos);
    camera.projMatrix = glm::perspective(glm::radians(perspective), (float)width / height, _near, _far);
    return camera;
}
