#include <iostream>
#include <sstream>
#include <vector>

#include "common/Application.hpp"
#include "common/LightInfo.hpp"
#include "common/Mesh.hpp"
#include "common/ShaderProgram.hpp"
#include "common/Texture.hpp"
#include "util.hpp"
#include "levelmap.hpp"
#include "gamecam.hpp"
#include "model.hpp"
#include "portal.hpp"


class LabApp : public Application {
public:
    LabApp(const std::string& folder, const std::string& level)
        : Application()
        , folder(folder)
        , levelMap(level)
        , useOverriddenCamera(false)
        , cameraType(CameraPlayer)
    {
    }

    void makeScene() override {
        Application::makeScene();

        setupShaders();
        setupLight();
        setupTextures();

        portalIn = Portal(512);
        portalOut = Portal(512);
        player = MovableBrick(0.2);

        setupLevel();

        setupSampler();

        setupCamera(CameraPlayer);
        setupModels();


    }

    void updateGUI() override {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize)) {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

            if (ImGui::CollapsingHeader("SpotLight")) {
                ImGui::ColorEdit3("ambient", glm::value_ptr(spotLight.ambient));
                ImGui::ColorEdit3("diffuse", glm::value_ptr(spotLight.diffuse));
                ImGui::ColorEdit3("specular", glm::value_ptr(spotLight.specular));
            }

            if (ImGui::CollapsingHeader("FloodLight")) {
                ImGui::ColorEdit3("FloodLight ambient", glm::value_ptr(floodLight.ambient));
                ImGui::ColorEdit3("FloodLight diffuse", glm::value_ptr(floodLight.diffuse));
                ImGui::ColorEdit3("FloodLight specular", glm::value_ptr(floodLight.specular));
            }

            ImGui::RadioButton("Free Camera", reinterpret_cast<int*>(&cameraType), CameraFree);
            ImGui::RadioButton("Player Camera", reinterpret_cast<int*>(&cameraType), CameraPlayer);
        }

        ImGui::End();
    }

    CameraInfo getCamera() {
        return (useOverriddenCamera) ? _overriddenCamera : _camera;
    }

    void draw() override {
        auto innerDraw = [&](bool draw_portals, bool draw_player = false, bool headlight = true) {
            onDrawGeneralHandler();
            onDrawShaderHandler(!headlight);


            for (auto& model : models) {
                drawModel(wallShader, model);
            }


            if (draw_portals && levelMap.portals.size()) {
                auto camera = getCamera();
                portalIn.move(levelMap.portals[0].x, levelMap.portals[0].y);

                if (USE_DSA) {
                    glBindTextureUnit(2, (levelMap.portals.size() == 2) ? portalOut.textureID : portalIn.textureID); // portalTextureTID);
                    glBindSampler(3, _sampler);
                } else {
                    assert(false);  // not implemented (not necessary)
                    glBindSampler(0, _sampler);
                    glActiveTexture(GL_TEXTURE0 + 0);
                    brickTexture->bind();
                }

                wallShader->setIntUniform("shaderTexture", 2);
                wallShader->setIntUniform("shaderNormal", 3);

                wallShader->setMat4Uniform("modelMatrix", portalIn.brick->modelMatrix());
                const auto normal = glm::transpose(
                        glm::inverse(glm::mat3(camera.viewMatrix * portalIn.brick->modelMatrix())));
                wallShader->setMat3Uniform("normalToCameraMatrix", normal);

                portalIn.brick->draw();

                if (levelMap.portals.size() == 2) {
                    portalOut.move(levelMap.portals[1].x, levelMap.portals[1].y);

                    if (USE_DSA) {
                        glBindTextureUnit(2, portalIn.textureID); // portalTextureTID);
                        glBindSampler(3, _sampler);
                    } else {
                        assert(false);  // not implemented (not necessary)
                        glBindSampler(0, _sampler);
                        glActiveTexture(GL_TEXTURE0 + 0);
                        brickTexture->bind();
                    }

                    wallShader->setIntUniform("shaderTexture", 2);
                    wallShader->setIntUniform("shaderNormal", 3);

                    wallShader->setMat4Uniform("modelMatrix", portalOut.brick->modelMatrix());
                    const auto normal = glm::transpose(
                            glm::inverse(glm::mat3(camera.viewMatrix * portalOut.brick->modelMatrix())));
                    wallShader->setMat3Uniform("normalToCameraMatrix", normal);

                    portalOut.brick->draw();
                }
            }

            if (draw_player) {
                auto camera = getCamera();
                player.move(_cameraMover->getPos().x, _cameraMover->getPos().y);


                if (USE_DSA) {
                    glBindTextureUnit(2, wallTexture->texture()); // portalTextureTID);
                    glBindSampler(3, _sampler);
                } else {
                    assert(false);  // not implemented (not necessary)
                    glBindSampler(0, _sampler);
                    glActiveTexture(GL_TEXTURE0 + 0);
                    brickTexture->bind();
                }

                wallShader->setIntUniform("shaderTexture", 2);
                wallShader->setIntUniform("shaderNormal", 3);

                wallShader->setMat4Uniform("modelMatrix", player.brick->modelMatrix());
                const auto normal = glm::transpose(
                        glm::inverse(glm::mat3(camera.viewMatrix * player.brick->modelMatrix())));
                wallShader->setMat3Uniform("normalToCameraMatrix", normal);


                player.brick->draw();
            }

            drawBlocks(brickTexture, bricksBlocks);
            drawBlocks(floorTexture, floorBlocks);
            drawBlocks(wallTexture, wallBlocks);


            drawMarkers();
        };


        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);


        if (levelMap.portals.size()) {
            _overriddenCamera = getPortalCameraInfo(glm::vec3(levelMap.portals[0].cam_x, levelMap.portals[0].cam_y, 0.0f),
                                                    glm::vec3(levelMap.portals[0].at_x, levelMap.portals[0].at_y, 0.0f),
                                                    glm::vec3(0.0f, 0.0f, 1.0f),
                                                    120.0f, 256, 256);
            useOverriddenCamera = true;

            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            glViewport(0, 0, portalIn.size, portalIn.size);
            glClearColor(0.0f, 0.0f, 1.0f, 1);
            // Портальный рендер
            innerDraw(false);
            // До копирования экрана в текстуру нужно указать её вызовом glBindTexture()
            glBindTexture(GL_TEXTURE_2D, portalIn.textureID);

            // Настал момент, которого мы ждали - мы рендерим экран в текстуру.
            // Передаем тип текстуры, детализацию, формат пиксела, x и y позицию старта,
            // ширину и высоту для захвата, и границу. Если вы хотите сохранитьтолько часть
            // экрана, это легко сделать изменением передаваемых параметров.
            glCopyTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 0, 0, portalIn.size, portalIn.size, 0);

            glBindSampler(0, 0);
            glUseProgram(0);

            useOverriddenCamera = false;

            if (levelMap.portals.size() == 2) {
                _overriddenCamera = getPortalCameraInfo(glm::vec3(levelMap.portals[1].cam_x, levelMap.portals[1].cam_y, 0.0f),
                                                        glm::vec3(levelMap.portals[1].at_x, levelMap.portals[1].at_y, 0.0f),
                                                        glm::vec3(0.0f, 0.0f, 1.0f),
                                                        120.0f, 256, 256);
                useOverriddenCamera = true;

                glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
                glViewport(0, 0, portalOut.size, portalOut.size);
                glClearColor(0.0f, 0.0f, 1.0f, 1);
                // Портальный рендер
                innerDraw(false);
                // До копирования экрана в текстуру нужно указать её вызовом glBindTexture()
                glBindTexture(GL_TEXTURE_2D, portalOut.textureID);

                // Настал момент, которого мы ждали - мы рендерим экран в текстуру.
                // Передаем тип текстуры, детализацию, формат пиксела, x и y позицию старта,
                // ширину и высоту для захвата, и границу. Если вы хотите сохранитьтолько часть
                // экрана, это легко сделать изменением передаваемых параметров.
                glCopyTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 0, 0, portalOut.size, portalOut.size, 0);

                glBindSampler(0, 0);
                glUseProgram(0);

                useOverriddenCamera = false;
            }
        }


        // Возвращаем экран в нормальный размер
        glViewport(0, 0, width, height);
        glClearColor(0.0f, 0.0f, 0.0f, 1);

        // Очищаем экран
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glBindTexture(GL_TEXTURE_2D, portalIn.textureID);

        // Пользовательский рендер
        innerDraw(true);


        glBindSampler(0, 0);
        glUseProgram(0);

        // Рисуем миникарту прям сверху
        glClear(GL_DEPTH_BUFFER_BIT);

        if (cameraType == CameraPlayer) {
            auto playerPos = _cameraMover->getPos();
            _overriddenCamera = getPortalCameraInfo(glm::vec3(playerPos.x, playerPos.y, 4.0f),
                                                    glm::vec3(playerPos.x, playerPos.y, 3.5f),
                                                    glm::vec3(0.0f, 1.0f, 0.0f),
                                                    120.0f, 256, 256);
            useOverriddenCamera = true;

            // Возвращаем экран в нормальный размер
            glViewport(0, 0, 256, 256);
            glClearColor(1.0f, 0.0f, 0.0f, 1);

            // Пользовательский рендер
            innerDraw(true, true, false);



            glBindSampler(0, 0);
            glUseProgram(0);

            useOverriddenCamera = false;
        }
    }


    // Camera
    TCameraType cameraType;
    TCameraType currentCameraType;
    
    CameraInfo _overriddenCamera;
    bool useOverriddenCamera;

    // Blocks
    std::vector<MeshPtr> wallBlocks;
    std::vector<MeshPtr> bricksBlocks;
    std::vector<MeshPtr> floorBlocks;

    // Shaders
    ShaderProgramPtr wallShader;
    ShaderProgramPtr markerShader;

    // Light
    MeshPtr lightMarker;
    LightInfo spotLight;
    LightInfo floodLight;

    // Textures
    TexturePtr wallTexture;
    TexturePtr brickTexture;
    TexturePtr floorTexture;

    TLevelMap levelMap;

    GLuint _sampler;

    std::string folder;
    std::vector<TModel> models;


    // Portal
    Portal portalIn;
    Portal portalOut;

    MovableBrick player;

private:
    void setupLevel();
    void setupLight();
    void setupTextures();
    void setupShaders();
    void setupCamera(const TCameraType& type);
    void setupSampler();
    void setupModels();

    void drawBlocks(const TexturePtr& texture, std::vector<MeshPtr> blocks);
    void drawMarkers();

    void onDrawGeneralHandler();
    void onDrawShaderHandler(bool ignoreSpotlight = false);

    void drawModel(ShaderProgramPtr shader, const TModel& model);
};