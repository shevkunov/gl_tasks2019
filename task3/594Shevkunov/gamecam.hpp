#pragma once

#include "common/Camera.hpp"
#include "levelmap.hpp"


enum TCameraType {
    CameraFree,
    CameraPlayer
};

class PlayerCameraMover : public CameraMover {
public:
    PlayerCameraMover(TLevelMap& levelMap);

    void handleKey(GLFWwindow* window, int key, int scancode, int action, int mods) override;
    void handleMouseMove(GLFWwindow* window, double xpos, double ypos) override;
    void handleScroll(GLFWwindow* window, double xoffset, double yoffset) override;
    void update(GLFWwindow* window, double dt) override;
    void openTeleport();

    virtual glm::vec3 getPos() override { return _pos; }
    glm::vec3 getDirection() override {
        return glm::vec3(0.0f, 0.0f, -1.0f) * _rot;
    }

    glm::vec3 _pos;

protected:
    glm::quat _rot;
    TLevelMap& _levelMap;

    //Положение курсора мыши на предыдущем кадре
    double _oldXPos = 0.0;
    double _oldYPos = 0.0;
    float _speed = 1.f;
    bool teleported;
    bool madeTeleportHere;
};

class FreeCameraSpeedableMover : public FreeCameraMover {
public:
    void update(GLFWwindow* window, double dt) override;
    float _speed = 1.f;
};