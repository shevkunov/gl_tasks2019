#pragma once

#include <iostream>
#include <sstream>
#include <vector>

#include "common/Application.hpp"
#include "common/LightInfo.hpp"
#include "common/Mesh.hpp"
#include "common/ShaderProgram.hpp"
#include "common/Texture.hpp"
#include "common/Camera.hpp"


#define GLM_ENABLE_EXPERIMENTAL

#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>
#include <imgui.h>


class Portal {
public:
    Portal() = default;

    Portal(int size, int channels = 3, int type = GL_RGB)
        : brick(makeCube(0.499f))
        , size(size)
    {
        // указатель для сохранения данных текстуры
        unsigned int *portalTexturePtr = nullptr;

        // Нужно создать пустую текстуру для рендера динамической текстуры.
        // Чтобы сделать это, просто создаем массив для хранинея данных и
        // передаем его OpenGL. Текстура хранится в видеокарте, так что мы можем
        // уничтожить массив в любой момент.
        // Эта функция принимает текстурный массив для сохранения текстуры,
        // размер текстуры, высоту и ширину, каналы (1,3,4), тип (RGB, RGBA, etc)
        // и текстурный ID.

        // Инициализируем память под массив текстуры и привяжем к pTexture
        portalTexturePtr = new unsigned int [size * size * channels];
        memset(portalTexturePtr, 0, size * size * channels * sizeof(unsigned int));


        // Зарегистрируем текстуру в OpenGL и привяжем к ID
        glGenTextures(1, &textureID);
        glBindTexture(GL_TEXTURE_2D, textureID);


        // Создаем текстуру и сохраняем в памяти

        // glTexImage2D(GLenum target,  GLint level,  GLint internalFormat,  GLsizei width,  GLsizei height,  GLint border,  GLenum format,  GLenum type,  const GLvoid * data);

        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, size, size, 0, GL_RGB, GL_UNSIGNED_BYTE, portalTexturePtr);

        // Устанавливаем качество
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);

        // Сохранив текстуру с пом. OpenGL, мы можем удалить временные данные
        delete [] portalTexturePtr;

        move(0.0f, 0.0f);
    }

    void move(float x, float y) {
        glm::vec3 blockCoordinates(x, y, 0.002f);
        brick->setModelMatrix(glm::translate(glm::mat4(1.0f), blockCoordinates));
    }

    unsigned int size;
    unsigned int textureID;
    MeshPtr brick;
};


class MovableBrick {
public:

    MovableBrick() = default;

    MovableBrick(float size)
        : brick(makeCube(size))
    {
        move(0.0f, 0.0f);
    }

    void move(float x, float y) {
        glm::vec3 blockCoordinates(x, y, 0.0f);
        brick->setModelMatrix(glm::translate(glm::mat4(1.0f), blockCoordinates));
    }

    MeshPtr brick;
};

CameraInfo getPortalCameraInfo(glm::vec3 pos, glm::vec3 center, glm::vec3 up, float perspective, float width, float height, float _near = 0.1f, float _far = 100.0f);