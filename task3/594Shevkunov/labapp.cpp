#include "labapp.hpp"


void LabApp::setupLevel() {
    for (int y = 0; y < levelMap.height; y++) {
        for (int x = 0; x < levelMap.width; x++) {
            switch (levelMap.get(y, x)) {
                case CellEmpty: {
                    MeshPtr floor = makeFloorBlock(0.5f);
                    glm::vec3 blockCoordinates(levelMap.begin.x + x, levelMap.begin.y + y, 0.0f);
                    floor->setModelMatrix(glm::translate(glm::mat4(1.0f), blockCoordinates));
                    floorBlocks.push_back(floor);
                    break;
                }
                case CellBrick: {
                    MeshPtr brick = makeCube(0.5f);
                    glm::vec3 blockCoordinates(levelMap.begin.x + x, levelMap.begin.y + y, 0.0f);
                    brick->setModelMatrix(glm::translate(glm::mat4(1.0f), blockCoordinates));
                    bricksBlocks.push_back(brick);
                    break;
                }
                case CellWall: {
                    MeshPtr wall = makeCube(0.5f);
                    glm::vec3 blockCoordinates(levelMap.begin.x + x, levelMap.begin.y + y, 0.0f);
                    wall->setModelMatrix(glm::translate(glm::mat4(1.0f), blockCoordinates));
                    wallBlocks.push_back(wall);
                    break;
                }
                default:
                    throw std::runtime_error("Unknown cell type");
            }
        }
    }
}

void LabApp::setupLight() {
    lightMarker = makeSphere(0.1f);

    spotLight.ambient = glm::vec3(1.0, 1.0, 0.1);
    spotLight.diffuse = glm::vec3(0.8, 0.8, 0.8);
    spotLight.specular = glm::vec3(1.0, 1.0, 1.0);

    floodLight.position = glm::vec3(0.0, 0.0, 5.0);
    floodLight.ambient = glm::vec3(0.4, 0.4, 0.4);
    floodLight.diffuse = glm::vec3(0.8, 0.8, 0.8);
    floodLight.specular = glm::vec3(1.0, 1.0, 1.0);
}

void LabApp::setupTextures() {
    wallTexture = loadTexture(folder + "bricks_white.tga");
    brickTexture = loadTexture(folder + "bricks_red.tga");
    floorTexture = loadTexture(folder + "ground_1024_v3_Q3.jpg");
}

void LabApp::setupShaders() {
    wallShader = std::make_shared<ShaderProgram>(folder + "texture.vert", folder + "texture.frag");
    markerShader = std::make_shared<ShaderProgram>(folder + "marker.vert", folder + "marker.frag");
}

void LabApp::setupCamera(const TCameraType& type) {
    switch (type) {
        case CameraFree:
            _cameraMover = std::make_shared<FreeCameraSpeedableMover>();
            break;
        case CameraPlayer:
            _cameraMover = std::make_shared<PlayerCameraMover>(levelMap);
    }
    currentCameraType = type;
}

void LabApp::setupSampler() {
    glGenSamplers(1, &_sampler);
    glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
}

void LabApp::setupModels() {
    auto randPos = [&](float h) -> glm::vec3 {
        int y, x;
        do {
            y = rand() % levelMap.height;
            x = rand() % levelMap.height;
        } while (levelMap.get(y, x) != CellEmpty);
        return glm::vec3(x - levelMap.width / 2., y - levelMap.height / 2., h);
    };

    for (int i = 0; i < 4; ++i) {

        models.emplace_back(TModel(folder + "models/Eyeball/eyeball.obj",
                                   folder + "models/Eyeball/textures/Eye_D.jpg",
                                   folder + "models/Eyeball/textures/Eye_N.jpg", randPos(0.), 0.1));

        models.emplace_back(TModel(folder + "models/obj/Only_Spider_with_Animations_Export.obj",
                                   folder + "models/obj/textures/Spinnen_Bein_tex.jpg",
                                   folder + "models/obj/textures/haar_detail_NRM.jpg", randPos(-0.5), 0.005));

        models.emplace_back(TModel(folder + "models/Skull.obj",
                                   folder + "models/tex/skull_diffuse.jpg",
                                   folder + "models/tex/skull_diffuse.jpg", randPos(-0.5), 0.1));
    }
}

void LabApp::drawBlocks(const TexturePtr& texture, std::vector<MeshPtr> blocks) {
    auto camera = getCamera();

    if (USE_DSA) {
        glBindTextureUnit(0, texture->texture());
        glBindSampler(0, _sampler);
    } else {
        glBindSampler(0, _sampler);
        glActiveTexture(GL_TEXTURE0 + 0);
        brickTexture->bind();
    }

    wallShader->setIntUniform("shaderTexture", 0);
    wallShader->setIntUniform("shaderNormal", 1);


    for (int i = 0; i < blocks.size(); i++) {
        wallShader->setMat4Uniform("modelMatrix", blocks[i]->modelMatrix());
        const auto normal = glm::transpose(glm::inverse(glm::mat3(camera.viewMatrix * blocks[i]->modelMatrix())));
        wallShader->setMat3Uniform("normalToCameraMatrix", normal);

        blocks[i]->draw();
    }
}

void LabApp::drawMarkers() {
    auto camera = getCamera();

    markerShader->use();

    markerShader->setMat4Uniform("mvpMatrix", camera.projMatrix * camera.viewMatrix * glm::translate(glm::mat4(1.0f), floodLight.position));
    markerShader->setVec4Uniform("color", glm::vec4(floodLight.diffuse, 1.0f));
    lightMarker->draw();
}

void LabApp::onDrawGeneralHandler() {
    if (currentCameraType != cameraType) {
        setupCamera(cameraType);
    }

    spotLight.position = _cameraMover->getPos();
}

void LabApp::onDrawShaderHandler(bool ignoreSpotlight) {
    auto camera = getCamera();

    auto getInCameraSpacePosition = [&](const glm::vec3 position) {
        return glm::vec3(camera.viewMatrix * glm::vec4(position, 1.0));
    };

    auto setLight = [&](const std::string& name, const LightInfo& light, const glm::vec3& direction,
                        bool override = false, const glm::vec3 overrideLa = glm::vec3(0, 0, 0)) {
        wallShader->setVec3Uniform(name + ".pos", getInCameraSpacePosition(light.position));
        wallShader->setVec3Uniform(name + ".direction", direction);
        wallShader->setVec3Uniform(name + ".La", override ? overrideLa : light.ambient);
        wallShader->setVec3Uniform(name + ".Ld", override ? overrideLa : light.ambient);
        wallShader->setVec3Uniform(name + ".Ls", light.specular);
    };

    wallShader->use();

    wallShader->setMat4Uniform("viewMatrix", camera.viewMatrix);
    wallShader->setMat4Uniform("projectionMatrix", camera.projMatrix);

    setLight("spotlight", spotLight, _cameraMover->getDirection(), ignoreSpotlight);
    if (ignoreSpotlight) {
        setLight("floodlight", floodLight, floodLight.position, true, glm::vec3(0.7, 0.7, 0.7));
    } else {
        setLight("floodlight", floodLight, floodLight.position);
    }
}


void LabApp::drawModel(ShaderProgramPtr shader, const TModel& model) {
    auto camera = getCamera();

    GLuint textureUnit = 4;
    GLuint normalUnit = 5;

    if (USE_DSA) {
        glBindTextureUnit(textureUnit, model.texture->texture());
        glBindSampler(textureUnit, _sampler);

        glBindTextureUnit(normalUnit, model.normals->texture());
        glBindSampler(normalUnit, _sampler);
    } else {
        glBindSampler(textureUnit, _sampler);
        glActiveTexture(GL_TEXTURE0 + textureUnit);
        model.texture->bind();

        glBindSampler(normalUnit, _sampler);
        glActiveTexture(GL_TEXTURE0 + normalUnit);
        model.normals->bind();
    }
    shader->setIntUniform("shaderTexture", textureUnit);
    shader->setIntUniform("shaderNormal", normalUnit);

    for (int i = 0; i < model.meshes.size(); i++) {
        shader->setMat4Uniform("modelMatrix", model.meshes[i]->modelMatrix());
        const auto normal = glm::transpose(glm::inverse(glm::mat3(camera.viewMatrix * model.meshes[i]->modelMatrix())));
        shader->setMat3Uniform("normalToCameraMatrix", normal);

        model.meshes[i]->draw();
    }
}