#pragma once

#include <fstream>
#include <vector>
#include <exception>
#include <iostream>

enum TCellType {
    CellWall,
    CellBrick,
    CellEmpty
};

class TLevelMap {
public:
    TLevelMap() = default;

    TLevelMap(const std::string& file) {
        std::ifstream fstream(file);
        fstream >> height >> width;

        begin = glm::vec2(-1.0f * width / 2.0f, -1.0f * height / 2.0f);

        map.assign(height, std::vector<TCellType>(width));
        for (auto& row : map) {
            for (auto& cell : row) {
                int id;
                fstream >> id;
                switch (id) {
                    case 0:
                        cell = CellBrick;
                        break;
                    case 1:
                        cell = CellEmpty;
                        break;
                    case 2:
                        cell = CellWall;
                        break;
                    default:
                        throw std::runtime_error("Unknow cell type");
                }
            }
        }
        fstream >> startX >> startY;
    }

    TCellType get(int y, int x) const {
        return map[y][x];
    }

    bool collide(const glm::vec2 ptr) const {
        auto inBound = [](int x, int bound) {
            return (0 <= x) && (x < bound);
        };
        const float delta = 0.1;
        for (float deltaX : {-delta, +delta}) {
            for (float deltaY : {-delta, +delta}) {
                glm::vec2 shifted = ptr - begin;
                int x = int(shifted.x + 0.5 + deltaX);
                int y = int(shifted.y + 0.5 + deltaY);
                if (inBound(y, height) && inBound(x, width) && (map[y][x] != CellEmpty)) {
                    return true;
                }
            }
        }
        return false;
    }

    std::vector<std::vector<TCellType>> map;
    glm::vec2 begin;
    int height;
    int width;

    int startX;
    int startY;
};