#include <iostream>
#include <sstream>
#include <vector>

#include "common/Application.hpp"
#include "common/LightInfo.hpp"
#include "common/Mesh.hpp"
#include "common/ShaderProgram.hpp"
#include "common/Texture.hpp"
#include "util.hpp"
#include "levelmap.hpp"
#include "gamecam.hpp"
#include "model.hpp"

class LabApp : public Application {
public:
    LabApp(const std::string& folder, const std::string& level)
        : Application()
        , folder(folder)
        , levelMap(level)
    {
    }

    void makeScene() override {
        Application::makeScene();

        setupShaders();
        setupLight();
        setupTextures();
        setupLevel();
        setupSampler();

        setupCamera(CameraPlayer);
        setupModels();
    }

    void updateGUI() override {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize)) {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

            if (ImGui::CollapsingHeader("SpotLight")) {
                ImGui::ColorEdit3("ambient", glm::value_ptr(spotLight.ambient));
                ImGui::ColorEdit3("diffuse", glm::value_ptr(spotLight.diffuse));
                ImGui::ColorEdit3("specular", glm::value_ptr(spotLight.specular));
            }

            if (ImGui::CollapsingHeader("FloodLight")) {
                ImGui::ColorEdit3("FloodLight ambient", glm::value_ptr(floodLight.ambient));
                ImGui::ColorEdit3("FloodLight diffuse", glm::value_ptr(floodLight.diffuse));
                ImGui::ColorEdit3("FloodLight specular", glm::value_ptr(floodLight.specular));
            }

            ImGui::RadioButton("Free Camera", reinterpret_cast<int*>(&cameraType), CameraFree);
            ImGui::RadioButton("Player Camera", reinterpret_cast<int*>(&cameraType), CameraPlayer);
        }

        ImGui::End();
    }

    void draw() override {
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);
        glViewport(0, 0, width, height);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        onDrawGeneralHandler();
        onDrawShaderHandler();


        for (auto& model : models) {
            drawModel(wallShader, model);
        }

        drawBlocks(brickTexture, bricksBlocks);
        drawBlocks(floorTexture, floorBlocks);
        drawBlocks(wallTexture, wallBlocks);

        drawMarkers();

        glBindSampler(0, 0);
        glUseProgram(0);
    }


    // Camera
    TCameraType cameraType;
    TCameraType currentCameraType;

    // Blocks
    std::vector<MeshPtr> wallBlocks;
    std::vector<MeshPtr> bricksBlocks;
    std::vector<MeshPtr> floorBlocks;

    // Shaders
    ShaderProgramPtr wallShader;
    ShaderProgramPtr markerShader;

    // Light
    MeshPtr lightMarker;
    LightInfo spotLight;
    LightInfo floodLight;

    // Textures
    TexturePtr wallTexture;
    TexturePtr brickTexture;
    TexturePtr floorTexture;

    TLevelMap levelMap;

    GLuint _sampler;

    std::string folder;
    std::vector<TModel> models;

private:
    void setupLevel();
    void setupLight();
    void setupTextures();
    void setupShaders();
    void setupCamera(const TCameraType& type);
    void setupSampler();
    void setupModels();

    void drawBlocks(const TexturePtr& texture, std::vector<MeshPtr> blocks);
    void drawMarkers();

    void onDrawGeneralHandler();
    void onDrawShaderHandler();

    void drawModel(ShaderProgramPtr shader, const TModel& model);
};