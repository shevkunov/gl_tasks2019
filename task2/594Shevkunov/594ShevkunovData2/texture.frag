/**
* Шейдер текстуры
*/

#version 330

uniform sampler2D shaderTexture;
uniform sampler2D shaderNormal;

// матрицы для преобразования координат
uniform mat4 modelMatrix; // локальная -> мировая
uniform mat4 viewMatrix; // мировая -> локальная
uniform mat4 projectionMatrix;  // камера -> усечённые координаты
uniform mat3 normalToCameraMatrix;

struct LightInfo
{
	vec3 pos;  //положение источника света в системе координат ВИРТУАЛЬНОЙ КАМЕРЫ!
	vec3 direction;  // направление источника света
	vec3 La;  //цвет и интенсивность окружающего света
	vec3 Ld;  //цвет и интенсивность диффузного света
	vec3 Ls;  //цвет и интенсивность бликового света
};
uniform LightInfo spotlight;
uniform LightInfo floodlight;

in vec3 normalCamSpace;  // нормаль в системе координат камеры
in vec4 posCamSpace;  // координаты вершины, система координат камеры
in vec2 texCoord;  //текстурные координаты

out vec4 fragColor;

const vec3 Ks = vec3(0.3, 0.3, 0.3);  // коэффициент бликового отражения
const float shininess = 128.0;

const float constAttenuation = 0.8;
const float linearAttenuation = 0.7;
const float quadraticAttenuation = 0.6;

void main()
{
	vec3 diffuseColor = texture(shaderTexture, texCoord).rgb;
	vec3 diffuseNormal = texture(shaderNormal, texCoord).rgb;
	vec3 normal = normalize(diffuseNormal * 2.0 - 1.0);
	vec3 viewDirection = normalize(-posCamSpace.xyz);
    vec4 spotlightDirCamSpace = viewMatrix * normalize(vec4(spotlight.direction, 0.0));  // мировая -> система камеры
	vec4 floodlightDirCamSpace = viewMatrix * normalize(vec4(floodlight.direction, 0.0));  // --//--

	float spotNdotL = max(dot(normal, spotlightDirCamSpace.xyz), 0.0);
	float floodNdotL = max(dot(normal, floodlightDirCamSpace.xyz), 0.0);

	float sourceDistance = distance(spotlight.pos, posCamSpace.xyz);
	float attenuation = 1.0f / (constAttenuation
		+ linearAttenuation * sourceDistance
        + quadraticAttenuation * sourceDistance * sourceDistance);

	vec3 color = diffuseColor * (floodlight.La + floodlight.Ld * floodNdotL);
	color += diffuseColor * (spotlight.La + spotlight.Ld * spotNdotL) * attenuation;

	if (spotNdotL > 0.0) {
		vec3 halfVector = normalize(spotlightDirCamSpace.xyz + viewDirection); // биссектриса между направлениями на камеру и на источник света
		float blinnTerm = max(dot(normal, halfVector), 0.0); // интенсивность бликового освещения
		blinnTerm = pow(blinnTerm, shininess);  // регулируем размер блика
		color += spotlight.Ls * Ks * blinnTerm * attenuation;
	}

	if (floodNdotL > 0.0) {  // ещё разок
		vec3 halfVector = normalize(floodlightDirCamSpace.xyz + viewDirection);
		float blinnTerm = max(dot(normal, halfVector), 0.0);
		blinnTerm = pow(blinnTerm, shininess);
		color += floodlight.Ls * Ks * blinnTerm;
	}

    fragColor = vec4(color, 1.0);
}