/**
* Шейдер для отрисовки маркера источника света
*/

#version 330

uniform mat4 mvpMatrix; // projectionMatrix @ viewMatrix @ modelMatrix
layout(location = 0) in vec3 localVertexPosition;

void main() {
	gl_Position = mvpMatrix * vec4(localVertexPosition, 1.0);
}
