#pragma once

#include "common/Mesh.hpp"
#include "common/Texture.hpp"
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <assimp/cimport.h>
#include <iostream>
#include <fstream>

class TModel {
public:
    TModel()  = default;

    TModel(const std::string& model, const std::string& texture, const std::string& normals, glm::vec3 pos, float scale = 1.)
        : texture(loadTexture(texture))
        , normals(loadTexture(normals))
    {
        glm::mat4 moveMat = glm::translate(glm::mat4(1.0f), pos);
        glm::mat4 scaleMat = glm::scale(moveMat, glm::vec3(scale));
        glm::mat4 modelMatrix = glm::rotate(scaleMat, 1.6f, glm::vec3(1.0f, 0.0f, 0.0f));

        loadModel(model, modelMatrix);
    }

    TexturePtr texture;
    TexturePtr normals;
    std::vector<MeshPtr> meshes;

private:
    void loadModel(const std::string& filename, glm::mat4 modelMatrix) {
        aiEnableVerboseLogging(true);
        auto stream = aiGetPredefinedLogStream(aiDefaultLogStream_STDOUT, nullptr);
        aiAttachLogStream(&stream);

        const struct aiScene* assimpScene = aiImportFile(filename.c_str(),
                                                         aiProcessPreset_TargetRealtime_MaxQuality);

        if (!assimpScene) {
            std::cerr << aiGetErrorString() << std::endl;
            return;
        }

        for (int meshIdx = 0; meshIdx < assimpScene->mNumMeshes; meshIdx++) {
            auto mesh = loadFromAIMesh(*assimpScene->mMeshes[meshIdx]);
            mesh->setModelMatrix(modelMatrix);
            meshes.push_back(mesh);
        }
        aiReleaseImport(assimpScene);
        aiDetachAllLogStreams();
    }
};
