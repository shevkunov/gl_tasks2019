#include "gamecam.hpp"

#define GLM_ENABLE_EXPERIMENTAL

#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>
#include <imgui.h>

#include <iostream>

PlayerCameraMover::PlayerCameraMover(const TLevelMap& levelMap)
    : CameraMover()
    , _pos(glm::vec3(levelMap.begin.x + levelMap.startX, levelMap.begin.y + levelMap.startY, 0.2))
    , _levelMap(levelMap)
{
    //Нам нужно как-нибудь посчитать начальную ориентацию камеры
    _rot = glm::toQuat(glm::lookAt(_pos, glm::vec3(0.0f, 0.0f, 0.5f), glm::vec3(0.0f, 0.0f, 1.0f)));
}

void PlayerCameraMover::handleKey(GLFWwindow* window, int key, int scancode, int action, int mods) {
}

void PlayerCameraMover::handleMouseMove(GLFWwindow* window, double xpos, double ypos) {
    int state = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);
    if (state == GLFW_PRESS)
    {
        double dx = xpos - _oldXPos;
        double dy = ypos - _oldYPos;

        //Добавляем небольшой поворот вверх/вниз
        glm::vec3 rightDir = glm::vec3(1.0f, 0.0f, 0.0f) * _rot;
        _rot *= glm::angleAxis(static_cast<float>(dy * 0.005), rightDir);

        //Добавляем небольшой поворов вокруг вертикальной оси
        glm::vec3 upDir(0.0f, 0.0f, 1.0f);
        _rot *= glm::angleAxis(static_cast<float>(dx * 0.005), upDir);
    }

    _oldXPos = xpos;
    _oldYPos = ypos;
}

void PlayerCameraMover::handleScroll(GLFWwindow* window, double xoffset, double yoffset) {
}

void PlayerCameraMover::update(GLFWwindow* window, double dt) {
    if (glfwGetKey(window, GLFW_KEY_EQUAL) == GLFW_PRESS) {
        _speed += .1f;
    }

    if (glfwGetKey(window, GLFW_KEY_MINUS) == GLFW_PRESS) {
        _speed -= .1f;
    }

    glm::vec3 forwDir = glm::vec3(0.0f, 0.0f, -1.0f) * _rot;
    glm::vec3 rightDir = glm::vec3(1.0f, 0.0f, 0.0f) * _rot;

    // Не двигаемся по высоте
    forwDir.z = 0;
    rightDir.z = 0;

    //Двигаем камеру вперед/назад
    auto newPos = _pos;

    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
        newPos += forwDir * _speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
        newPos -= forwDir * _speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
        newPos -= rightDir * _speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
        newPos += rightDir * _speed * static_cast<float>(dt);
    }

    if (!_levelMap.collide(glm::vec2(newPos.x, newPos.y))) {
        _pos = newPos;
    }

    //-----------------------------------------

    //Соединяем перемещение и поворот вместе
    _camera.viewMatrix = glm::toMat4(-_rot) * glm::translate(-_pos);

    //-----------------------------------------

    int width, height;
    glfwGetFramebufferSize(window, &width, &height);

    //Обновляем матрицу проекции на случай, если размеры окна изменились
    _camera.projMatrix = glm::perspective(glm::radians(45.0f), (float)width / height, _near, _far);
}



void FreeCameraSpeedableMover::update(GLFWwindow* window, double dt) {
    if (glfwGetKey(window, GLFW_KEY_EQUAL) == GLFW_PRESS) {
        _speed += .1f;
    }

    if (glfwGetKey(window, GLFW_KEY_MINUS) == GLFW_PRESS) {
        _speed -= .1f;
    }

    //Получаем текущее направление "вперед" в мировой системе координат
    glm::vec3 forwDir = glm::vec3(0.0f, 0.0f, -1.0f) * _rot;

    //Получаем текущее направление "вправо" в мировой системе координат
    glm::vec3 rightDir = glm::vec3(1.0f, 0.0f, 0.0f) * _rot;

    //Двигаем камеру вперед/назад
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
        _pos += forwDir * _speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
        _pos -= forwDir * _speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
        _pos -= rightDir * _speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
        _pos += rightDir * _speed * static_cast<float>(dt);
    }

    //-----------------------------------------

    //Соединяем перемещение и поворот вместе
    _camera.viewMatrix = glm::toMat4(-_rot) * glm::translate(-_pos);

    //-----------------------------------------

    int width, height;
    glfwGetFramebufferSize(window, &width, &height);

    //Обновляем матрицу проекции на случай, если размеры окна изменились
    _camera.projMatrix = glm::perspective(glm::radians(45.0f), (float)width / height, _near, _far);
}
